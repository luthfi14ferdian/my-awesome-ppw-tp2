from django.shortcuts import render

response = {}

def index(request):
    response['author'] = 'Luthfi Ferdian'
    return render(request, "profile/profile.html", response)
